﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Linq;

namespace Kalina_Kursovaya_variant_7_PticeFabrika
{
     class PoultryFarm
    {
        public int ChickenCell { get; set; }
        public string Date { get; set; }
        public bool HaveEgg { get; set; }
        public PoultryFarm(int ChickenCell, string Date, bool HaveEgg)
        {
            this.ChickenCell = ChickenCell;
            this.Date = Date;
            this.HaveEgg = HaveEgg;
        }
        public string info()
        {
            string information = ChickenCell + "|" + Date + "|" + HaveEgg;
            return information;
        }
    }
    class Chicken
    {
        public static int id { get; set; }
        public int Age { get; set; }
        public int Weight { get; set; }
        public int EggProduction { get; set; }
        public string Bring { get; set; }
        public int Key { get; set; }
        public Chicken(int age, int weight, int eggProduction, string bring)
        {
            this.Age = age;
            this.Weight = weight;
            this.EggProduction = eggProduction;
            this.Bring = bring;
            this.Key = id++;
        }
        public string info()
        {
            string information = Key + "|" + Age + "|" + Weight + "|" + EggProduction + "|" + Bring;
            return information;
        }
    }
    class Employe
    {
        public string Name { get; set; }
        public int Selery { get; set; }
        public static int id { get; set; }
        public int Key { get; set; }
        public string[] Cells { get; set; }
        public Employe(string name, int selery, string[] Cells)
        {
            this.Name = name;
            this.Selery = selery;
            this.Cells = Cells;
            this.Key = id++;
        }
        public string info()
        {
            string information = Key + "|" + Name + "|" + Cells[0] + ";" +Cells[1] + ";" +Cells[2] + ";" +Cells[3] + ";" + "|" + Selery;
            return information;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {

            List<Chicken> chickens = new List<Chicken>();
            string path = @"/home/kalin/Documents/c#/kursovaya_poultryfarm/Bid_Data/Chicken.txt";
            using(StreamReader sr = new StreamReader(path,System.Text.Encoding.Default))
            {
                string line;
                while((line=sr.ReadLine())!=null)
                {
                    string[] words = line.Split(new char[] { '|' });
                    chickens.Add(new Chicken(Convert.ToInt32(words[1]),Convert.ToInt32(words[2]),Convert.ToInt32(words[3]),words[4]));
                }
            }
            bool end = true;
            while(end){
                Console.Write("1.Average eggs of chikens with certain age and weight\n");
                Console.Write("2.Chickens that egg production less average egg production on poulty farm \n");
                Console.Write("3.Chicken that egg production best of poulty farm \n");
                Console.Write("4.Add chicken\n");
                Console.Write("5.Remove chicken\n");
                Console.Write("6.Say by-by \n");

                switch(Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        Console.Write("How age: \n");
                        int age = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Minimal weight: \n");
                        int minWeight = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Max weight: \n");
                        int maxWeight = Convert.ToInt32(Console.ReadLine());
                        try
                        {
                        var selected = chickens.Where(c => c.Age == age && c.Weight > minWeight && c.Weight < maxWeight).Average(c => c.EggProduction);
                        Console.WriteLine("Average: " + Convert.ToInt32(selected));
                        }
                        catch
                        {
                            Console.WriteLine("Sorry we Found off");
                        }
                        break;
                    case 2:
                        var avg = chickens.Select(c => c).Average(c => c.EggProduction);
                        var selectedAvg = chickens.Where(c => c.EggProduction < Convert.ToInt32(avg)).Select(c => c.info());
                        foreach(var c in selectedAvg)
                        {
                         Console.WriteLine(c);   
                        }
                        break;
                    case 3:
                        var MAXEGG = chickens.Max(c => c.EggProduction);
                        var THEBIGCHICKEN = chickens.Where(c => c.EggProduction == MAXEGG).Select(c => c.info());
                        foreach(var c in THEBIGCHICKEN)
                        {
                            Console.WriteLine("Numb of cell: " + c +" \n");
                        }
                        break;
                    case 4:
                        Console.Write("Write weight: \n");
                        int _weight = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Write age: \n");
                        int cAge = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Write eggproduction: \n");
                        int _eggproduction = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Write bring: \n");
                        string _bring = Console.ReadLine();
                        chickens.Add(new Chicken(cAge, _weight, _eggproduction, _bring));
                        using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default))
                        {
                                sw.WriteLine(chickens[chickens.Count() - 1].info());
                        }
                        break;
                    case 5:
                        chickens.RemoveAt(Convert.ToInt32(Console.ReadLine()));
                        using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                        {
                            foreach (Chicken i in chickens)
                            {
                                sw.WriteLine(i.info());
                            }
                        }
                        break;
                    case 6:
                        end = false;
                        break;                        
                }
            }
        }
    }
}
